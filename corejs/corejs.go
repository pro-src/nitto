/*
Package corejs contains the source for the JavaScript utility-belt library.

	import (
		_ "github.com/pro-src/otto/corejs"
	)
	// Every Otto runtime will now include corejs

https://github.com/zloirock/core-js

https://unpkg.com/core-js-bundle@3.0.1/minified.js

By importing this package, you'll automatically load corejs every time you create a new Otto runtime.

To prevent this behavior, you can do the following:

	import (
		"github.com/pro-src/otto/corejs"
	)

	func init() {
		corejs.Disable()
	}

*/
package corejs

import (
	"github.com/robertkrimen/otto/registry"
)

var entry *registry.Entry = registry.Register(func() string {
	return Source()
})

// Enable corejs runtime inclusion.
func Enable() {
	entry.Enable()
}

// Disable corejs runtime inclusion.
func Disable() {
	entry.Disable()
}

// Source returns the corejs source.
func Source() string {
	return string(corejs())
}

