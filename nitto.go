package main

import (
	"fmt"
	"errors"
	"time"
	"os"
	"encoding/base64"

	"github.com/robertkrimen/otto"
        _"github.com/pro-src/nitto/corejs"
        "C"
)


var halt = errors.New("Script execution halted.")

func main() {}

//export run
func run(src *C.char) *C.char {
	defer func() {
		if caught := recover(); caught != nil {
			if caught == halt {
				fmt.Fprintf(os.Stderr, "Script execution took to long!\n")
			}
			panic(caught)
		}
	}()

	vm := otto.New()
	vm.Set("atob", func (call otto.FunctionCall) otto.Value {
		data, err := base64.StdEncoding.DecodeString(call.Argument(0).String())
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			panic(err)
		}
		result, _ := vm.ToValue(string(data))
		return result
	})
	vm.Interrupt = make(chan func(), 1) // The buffer prevents blocking

	go func() {
		time.Sleep(4 * time.Second)
		vm.Interrupt <- func() {
			panic(halt)
		}
	}()

	result, _ := vm.Run(C.GoString(src))
	return C.CString(result.String())
}

