import os

from ctypes import CDLL, c_char_p

__version__ = '0.0.3'

nitto = CDLL(os.path.join(os.path.dirname(__file__), 'lib', 'nitto.so'))

run = nitto.run
run.argtypes = (c_char_p,)
run.restype = c_char_p
