"""setuptools/distutils based setup script for nitto.

Copyright (c) 2019 Rodney Teal (@pro-src)
All rights reserved.

This file is part of nitto and is released under
the "MIT License Agreement". Please see the LICENSE file
that should have been included as part of this package.

Example:
  python setup.py bdist_wheel --pkg-os=linux,windows,darwin
  python setup.py bdist_wheel --pkg-arch=amd64,386,arm64,arm
  python setup.py bdist_wheel --pkg-os=windows,darwin --pkg-arch=amd64
  python setup.py bdist_wheel --pkg-os=linux --pkg-arch=amd64,386
"""

import os
import re
import hashlib

from setuptools import setup
from setuptools.command.build_py import build_py
from distutils.util import get_platform
from distutils.command.clean import clean
from wheel.bdist_wheel import bdist_wheel
from subprocess import PIPE, Popen
from datetime import datetime
from shutil import rmtree
from io import open


package = dict(os=[], arch=[])

repo_root = os.path.dirname(os.path.abspath(__file__))
init_file = os.path.join(repo_root, 'nitto', '__init__.py')

with open(init_file, 'r', encoding='utf-8') as f:
    version = re.search('__version__ = ["\'](.+?)["\']', f.read()).group(1)


def cmdline(command, stdout=PIPE, **kwargs):
    ret = Popen(command, stdout=stdout, shell=True, **kwargs).communicate()[0]
    return ret and ret.strip()


def matches(sub, strings):
    """Returns True if sub is a substring of any item in the list"""
    return any(sub in item for item in strings)


def allows(sub, strings):
    """Similar to `matches` but returns True if list is empty"""
    return not len(strings) or any(sub == item for item in strings)


def x386():
    return (matches('86', package['arch'])
            and not matches('64', package['arch']))

class custom_clean(clean):
    def run(self):
        clean.run(self)
        if self.all:
            os.system('rm -vrf ./*.pyc ./*.tgz ./*.egg-info')

    def forceRun(self):
        self.all = True
        self.finalize_options()
        self.run()


class custom_build(build_py):
    def run(self):
        # Fresh build on each run
        custom_clean(self.distribution).forceRun()

        if os.getcwd() != repo_root:
            raise ValueError(
                'You are not in the root of the repo.\n'
                'Please cd to "%s" before running this script.' %
                root)

        go = (
            'go build -a -installsuffix cgo'
            ' -buildmode=c-shared -ldflags="{ldflags}"'
            ' -o "{target}" "{source}"'
        )

        ldflags = " ".join((
            '-s',
            '-w',
            '-X', 'main.commitHash=%s' % cmdline('git rev-parse --short HEAD'),
            '-X', 'main.buildDate=%s' % datetime.now().strftime('%Y-%m-%d'),
            '-X', 'main.version=%s' % version
        ))

        platforms = {
            'linux': {
                'archs': ('amd64', '386', 'armv6', 'armv7'),
                'ext': '.so'
            },
            'windows': {
                'archs': ('amd64', '386'),
                'ext': '.dll'
            },
            'darwin': {
                'archs': ('amd64', '386'),
                'ext': '.dylib'
            }
        }

        pkg_dir = os.path.join(repo_root, 'nitto')
        lib_dir = os.path.join(pkg_dir, 'lib')
        sum_dir = os.path.join(repo_root, 'checksums')

        if os.path.exists(lib_dir):
            rmtree(lib_dir)

        for x in (lib_dir, sum_dir):
            try:
                os.makedirs(x)
            except OSError as e:
                if not e.errno == os.errno.EEXIST or not os.path.isdir(x):
                    raise

        for k in platforms:
            if not allows(k, package['os']):
                continue
            for arch in platforms[k]['archs']:
                if not allows(arch, package['arch']):
                    continue

                libname = 'nitto-%s-%s%s' % (k, arch, platforms[k]['ext'])
                target = os.path.join(pkg_dir, 'lib', libname)

                env = os.environ.copy()
                if arch == 'armv6':
                    arch = 'arm'
                    env.update(CC='arm-linux-gnueabi-gcc', GOARM='6')
                if arch == 'armv7':
                    arch = 'arm'
                    env.update(CC='arm-linux-gnueabi-gcc', GOARM='7')
                if k == 'windows':
                    if x386():
                        env.update(CC='x86_64-w64-mingw32-gcc-8.3-win32')
                    else:
                        env.update(CC='x86_64-w64-mingw32-gcc')
                env.update(GOARCH=arch, GOOS=k, CGO_ENABLED='1')

                build_cmd = go.format(
                    ldflags=ldflags,
                    target=target,
                    source=os.path.join(repo_root, 'nitto.go')
                )

                print(build_cmd)
                cmdline(build_cmd, stdout=None, env=env)

                with open(target, 'rb') as f:
                    hash = hashlib.sha256(f.read()).hexdigest()
                with open(os.path.join(sum_dir, '%s.sha256' % libname), 'w+') as f:
                    f.write(('%s  %s\n' % (hash, libname)).decode('utf-8'))

        build_py.run(self)


class custom_wheel(bdist_wheel):
    user_options = bdist_wheel.user_options + [
        ('pkg-os=', None, 'Package platforms'),
        ('pkg-arch=', None, 'Package architectures')
    ]

    def initialize_options(self):
        bdist_wheel.initialize_options(self)
        self.pkg_os = self.pkg_arch = ''

    def get_tag(self):
        tag = bdist_wheel.get_tag(self)

        if not matches('darwin', package['os']):
            return tag

        # Replace the original osx platform tag
        if x386():
            osx_tag = 'macosx_10_6_intel'
        else:
            osx_tag = (
                'macosx_10_6_intel.'
                'macosx_10_9_intel.'
                'macosx_10_9_x86_64.'
                'macosx_10_10_intel.'
                'macosx_10_10_x86_64'
            )

        return (tag[0], tag[1], osx_tag)

    def finalize_options(self):
        bdist_wheel.finalize_options(self)

        if self.pkg_os:
            package['os'].extend(self.pkg_os.split(','))
        if self.pkg_arch:
            package['arch'].extend(self.pkg_arch.split(','))

        self.universal = True
        self.plat_name_supplied = True

        if allows('linux', package['os']):
            if matches('arm', package['arch']):
                if len(package['arch']) > 1:
                    raise ValueError('Multiple arm versions')
                self.plat_name = 'linux_%s1' % package['arch'][0]
            else:
                self.plat_name = 'linux_i386' if x386() else 'linux_x86_64'
        elif matches('windows', package['os']):
            self.plat_name = 'win32' if x386() else 'win_amd64'
        else:
            self.plat_name = get_platform()


setup(
    name='nitto',
    version=version,
    cmdclass={
        'clean': custom_clean,
        'build_py': custom_build,
        'bdist_wheel': custom_wheel
    },
    packages=['nitto'],
    include_package_data=True
)
